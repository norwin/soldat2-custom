# Soldat 2 custom content

This repo contains some custom mods and maps I wrote, took from the [Soldat 2][soldat] [discord][discord], or found elsewhere.
Its purpose is to
- simplify server hosting for me,
- distribute mods & maps I wrote,
- track the default configuration of the game, so I can [diff the changes between versions][blame] properly.

## usage for clients
- linux command line:
    ```sh
    git clone https://codeberg.org/norwin/soldat2-custom /tmp/soldat2-custom
    (cd /tmp/soldat2-custom && tar c .) | (cd ~/.steam/steam/steamapps/common/Soldat\ 2/ && tar x)
    ```
- windows: download [this zip][zip], unpack & merge it into your soldat steam folder (steam right click > manage > browse local files)

## usage for servers
1. download soldat server
    ```sh
    wget https://dl.thd.vg/soldat2-linuxserver-release.tar.gz
    tar xzf soldat2-linuxserver-release.tar.gz
    ```
2. download this content & select a version
    ```sh
    git clone https://codeberg.org/norwin/soldat2-custom
    pushd soldat2-custom
    git branch --remotes --verbose
    git checkout server/<name>
    popd
    ```
3. deploy config
    ```sh
    rsync --archive --delete --exclude Scripts/Standard soldat2-custom/* soldat2-linuxserver-release
    ```
4. run it
    ```sh
    useradd soldat
    chown -R soldat soldat2-linuxserver-release
    sudo -u soldat soldat2-linuxserver-release/soldat2
    ```

[soldat]: https://soldat2.com/
[discord]: https://discord.gg/Ar3QM7d
[zip]: https://codeberg.org/norwin/soldat2-custom/archive/main.zip
[blame]: https://codeberg.org/norwin/soldat2-custom/blame/branch/main/Modifiers/_default.json

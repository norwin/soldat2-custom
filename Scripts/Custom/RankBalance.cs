using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Teal;

/*
 * Balance team games based on the ranks on match start and/or on !bal command.
 * author: @noerw
 * script version: 0.1 (beta)
 * target game version: 0.8.60
 */

[RequireComponent(typeof(Match))]
[RequireComponent(typeof(Teams))]
[DisallowMultipleComponent]
public class RankBalance : Modifiable
{
    [JsonProperty("BalanceOnMatchStart")] public bool balanceOnStart = true;
    [JsonProperty("BalanceOnCommand")] public bool balanceOnCommand = true;
    [JsonProperty("BalanceBots")] public bool balanceBots = true;

    private void Awake() {
        GameChat.instance.OnChat.AddListener(OnPlayerChat);
        if (balanceOnStart)
            BalanceTeams();
    }

    private void OnDestroy() {
        GameChat.instance.OnChat.RemoveListener(OnPlayerChat);
    }

    void OnPlayerChat(Player p, string msg) {
        if (p == null || !balanceOnCommand && (int)p.props["seclev"] == (int)Player.Seclev.Regular)
            return;
        if(msg != "!bal" && msg != "!balance")
            return;

        // FIXME: when in a match, this should try to minimize team switches.
        // i.e. try both initial targetTeam 0 and 1, and compare with existing teams.
        BalanceTeams();
    }

    void BalanceTeams() {
        // This function ignores existing state, and prioritizes team size over optimal
        // rank distribution.
        // NOTE: balancing by rank is a https://en.wikipedia.org/wiki/Subset_sum_problem

        if (!Net.IsServer) return;
        if (Teams.instance.GetTeamsCount() != 2) return;

        List<Player> players = MonoSingleton<Players>.Get.GetPlayersNonSpecators();
        if (!balanceBots)
            players = MonoSingleton<Players>.Get.GetHumansNonSpectator();

        if (players.Count < 3) // balancing doesn't make sense with < 3 players
            return;

        // do the balancing by putting best & worst unassigned players pairwise
        // into a team. this results very often in the optimal solution, but not always.
        // even when not, delta-to-optimal is in an acceptable 15% range.
        int  minTeamSize = players.Count / 2; // NOTE: deliberate integer division
        int   targetTeam = 0;
        int[] sumOfRanks = new int[2];
        players.Sort((p1, p2) => GetRank(p2) - GetRank(p1));
        for(int i = 0; i < minTeamSize; i++) {
            // if team size is uneven, don't put the last pair in the same team, but put each in a different one.
            // prefer putting the better player in team 1, as team 0 already got the best player.
            bool isLastPairUnevenTeamsize = (i == minTeamSize-1 && minTeamSize%2 == 1);
            if (isLastPairUnevenTeamsize)
                targetTeam = NextTeam(targetTeam);

            AssignTeam(players[i], targetTeam);
            sumOfRanks[targetTeam] += GetRank(players[i]);

            if (minTeamSize > 1) {
                if (isLastPairUnevenTeamsize)
                    targetTeam = NextTeam(targetTeam);

                Player p2 = players[2*minTeamSize-1-i];
                AssignTeam(p2, targetTeam);
                sumOfRanks[targetTeam] += GetRank(p2);
            }
            targetTeam = NextTeam(targetTeam);
        }

        // if there is an uneven amount of players, put the remaining lowest ranked into the weaker team
        bool teamsEvenlySized = players.Count % 2 == 0;
        if (!teamsEvenlySized) {
            Player lowest = players[players.Count-1];
            // if team 0 is stronger, or teams are equal (team 0 got highest ranking
            // player, so put them in team 1)
            AssignTeam(lowest, sumOfRanks[0] - sumOfRanks[1] >= 0 ? 1 : 0);
        }

        // report results
        double relativeRankDelta = (
            (double)(sumOfRanks[0] - sumOfRanks[1]) /
            (double)(sumOfRanks[0] + sumOfRanks[1])
        );
        string not = teamsEvenlySized ? "" : "NOT ";
        GameChat.instance.ServerChat($"Teams balanced based on ranks.");
        GameChat.instance.ServerChat($"Teams are {not}evenly sized, rank diff is {100 * relativeRankDelta:F0}%");
    }

    // ↓ ↓ HELPERS ↓ ↓

    void AssignTeam(Player p, int team) { p.props["team"] = team; }

    int NextTeam(int team) { return (team + 1) % 2; }

    int GetRank(Player p) {
        // rank is the MatchmakingAPI tier from 1-18.
        // if a player (or bot) is unranked, this returns 0 - to make these players count better, we increment the rank by 3.
        // NOTE: its not clear which rank this is.. probably CTF-Standard-6
        // FIXME: this value is set a couple seconds after a player has joined (API latency is bad...), it returns 0 in the meantime.
        // HACK: the property is a System.byte... conversion to string works, but to int throws an out of bounds exception, so we roundtrip..
        if (p.IsBot()) return 0;
        return System.Int32.Parse(p.props["rank"].ToString()) + 3;
    }
}

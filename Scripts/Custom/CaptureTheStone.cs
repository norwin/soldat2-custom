﻿using System.Linq;
using System.Collections;
using UnityEngine;
using Newtonsoft.Json;
using Teal;

/*
 * author: @noerw, @AL
 * script version: 0.3
 * target game version: 0.8.60
 */

[RequireComponent(typeof(Match))]
[RequireComponent(typeof(Teams))]
[DisallowMultipleComponent]
public class CaptureTheStone: MonoBehaviour
{
    [JsonProperty("FlaggerHasNoJets")] public bool flaggerNoJets = true;
    [JsonProperty("FlagDroppedNoJetsSeconds")] public float flaggerNoJetsDelay = 0.9F;
    [JsonProperty("FlaggerMassAdd")] public float flaggerMassAdd = 0.14F;

    private void Awake()
    {
        Eventor.AddListener(Events.Flag_Grabbed, OnFlagGrabbed);
        Eventor.AddListener(Events.Item_Dropped, OnItemDrop);
    }

    private void OnDestroy()
    {
        Eventor.RemoveListener(Events.Flag_Grabbed, OnFlagGrabbed);
        Eventor.RemoveListener(Events.Item_Dropped, OnItemDrop);
    }

    void OnFlagGrabbed(IGameEvent ev)
    {
        Controls flagger = GetFlagHolder(ev.Sender);
        if (!flagger)
            return;

        if (flaggerNoJets)
        {
            GostekJets jets = flagger.GetComponent<GostekJets>();
            jets.jetsAllowed = false;
            jets.JetsOff();
        }
        if (flaggerMassAdd!= 0.0) {
            PhysicsObject phys = flagger.GetComponent<PhysicsObject>();
            phys.mass += flaggerMassAdd;
        }
    }

    void OnItemDrop(IGameEvent ev)
    {
        Flag flag = ev.Sender.GetComponent<Flag>();
        if (!flag)
            return;

        Controls flagger = GetFlagHolder(ev.Sender);

        if (flaggerMassAdd != 0.0)
            flagger.GetComponent<PhysicsObject>().mass -= flaggerMassAdd;

        if (flaggerNoJets)
            StartCoroutine(ReenableJetsDelayed(flagger));
    }

    IEnumerator ReenableJetsDelayed(Controls player)
    {
        yield return new WaitForSeconds(flaggerNoJetsDelay);
        if (player.GetComponentInChildren<Flag>() == null)
            player.GetComponent<GostekJets>().jetsAllowed = true;
    }

    Controls GetFlagHolder(GameObject flagObject)
    {
        Flag flag = flagObject.GetComponent<Flag>();
        ItemPickup holder = flag.lastholder;
        if (holder)
            return holder.GetComponent<Controls>();
        return null;
    }
}

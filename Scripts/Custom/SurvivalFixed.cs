﻿using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using Teal;

/*
 * author: @AL
 * script version: 0.2
 * target game version: 0.8.60
 */

[DisallowMultipleComponent]
public class SurvivalFixed : Modifiable
{
    Teams teams;
    Match match;
    Respawning respawning;
    StandardRespawning standardRespawning;
    StandardEndConditions endConditions;

    enum Mode {
        UNKNOWN,
        DM,
        TDM,
        CTF
    }
    Mode gamemode;
    bool ended;
    int round;
    int[] rounds_won = new int[4];
    int opt;

    [JsonProperty("MaxRounds")] public int max_rounds = 0;
    [JsonProperty("SurvivalBonus")] public int survival_bonus = 1;
    [JsonProperty("CelebrationTime")] public float celebration_time = 5.0f;
    [JsonProperty("ResetTime")] public float reset_time = 5.0f;

    void Awake()
    {
        Eventor.AddListener(Events.Died, OnDied);
        Eventor.AddListener(Events.Match_Started, OnStart);
        Eventor.AddListener(Events.Flag_Captured, OnFlagCap);
        Eventor.AddListener(Events.Player_Left, CheckRound);
        Eventor.AddListener(Events.Player_Changed_Team, CheckRound);
    }

    private void OnDestroy()
    {
        Eventor.RemoveListener(Events.Died, OnDied);
        Eventor.RemoveListener(Events.Match_Started, OnStart);
        Eventor.RemoveListener(Events.Flag_Captured, OnFlagCap);
        Eventor.RemoveListener(Events.Player_Left, CheckRound);
        Eventor.RemoveListener(Events.Player_Changed_Team, CheckRound);
    }

    private void Start()
    {
        match = GetComponent<Match>();
        teams = GetComponent<Teams>();
        respawning = GetComponent<Respawning>();
        standardRespawning = GetComponent<StandardRespawning>();
        standardRespawning.allowRespawning = false;

        match.multiplayerOnlyWarmup = false;
        match.warmupSecs = Mathf.Max(match.warmupSecs, 5);

        ended = false;
        round = 0;
        for( int i=0; i<rounds_won.Length; i++ ) rounds_won[i]=0;
        switch(Scoreboard.Get.gamemodeText.text) {
            case "DEATHMATCH":
                gamemode = Mode.DM;
                break;
            case "TEAM DEATHMATCH":
                gamemode = Mode.TDM;
                break;
            case "CAPTURE THE FLAG":
                gamemode = Mode.CTF;
                break;
            default:
                // for anything else, try to classify based on teams used
                if( teams == null || teams.teams.Count < 2 ) {
                    gamemode = Mode.DM;
                } else if( teams && teams.teams.Count >= 2 ) {
                    gamemode = Mode.TDM;
                }
                break;
        }
    }

    void Update()
    {
        if( opt++ % 10 == 1 ) {
            if( gamemode==Mode.TDM )
                for( int i=0; i<teams.teams.Count; i++ )
                    teams.teams[i].kills = rounds_won[i];
        }
    }

    void OnStart(IGameEvent e)
    {
        foreach( Player p in Players.Get.players ) {
            if (respawning.IsPlayerInQueue(p.id)) {
                Respawning.RespawnObject ro = respawning.GetPlayerQueue(p.id);
                ro.timeOfAdd = RealTime.realtimeSinceStartup;
                ro.waitSecs = 0.1f;
            }
        }
    }

    void OnFlagCap(IGameEvent e)
    {
        Timing.RunCoroutine(EndRound(0.0f,true));
    }

    bool ResetNeeded()
    {
        if( gamemode==Mode.DM ) {
            List<Player> palive = Players.Get.GetAlive();
            if( palive.Count<=1 ) return true;
        } else {
            int talive = 0;
            for( int i=0; i<teams.teams.Count; i++ ) {
                List<Player> palive = Players.Get.GetAliveTeam(i);
                if( palive.Count>0 ) talive++;
            }
            if( talive<=1 ) return true;
        }
        return false;
    }

    void CheckRound(IGameEvent e)
    {
        if( !ended && ResetNeeded() )
            Timing.RunCoroutine(EndRound(0.0f));
    }

    private void OnDied(IGameEvent e)
    {
        if( !ended && ResetNeeded() )
            Timing.RunCoroutine(EndRound(celebration_time));

        if (this == null || !gameObject.activeInHierarchy)
            return;
        Controls controls = e.Sender.GetComponent<Controls>();
        if (controls && controls.IsLocal()) {
            Timing.RunCoroutine(DeadText(controls.player));
        }
    }

    IEnumerator<float> DeadText(Player player)
    {
        yield return Timing.WaitForSeconds(3.0f);

        if (this == null || Game.IsLoadingLevel || GameCamera.Get == null)
            yield break;

        if (player && player.IsDead())
            HUD.Get.Powerup("Waiting for round end...", -1.0f);

        GameCamera.Get.Target = null; // spectate
        GameCamera.Get.spectatorWhenDead = true;
    }

    IEnumerator<float> EndRound(float kill_delay, bool capped=false)
    {
        if( ended || this==null || Game.IsLoadingLevel || GameCamera.Get==null )
            yield break;

        ended = true;
        round += 1;
        List<Player> palive = Players.Get.GetAlive();

        /*
        // increment score for winning team
        // NOTE: is this really wanted? round ends super fast with this.
        //   -> could increase score limit, but then killing has equal value as capping
        if( !capped && (gamemode==Mode.CTF) ) {
            foreach( TeamData td in teams.teams ) {
                if( Players.Get.GetAliveTeam(td.Number).Count>0 )
                    td.score += 1;
            }
        }
        */

        if( kill_delay>0 )
            yield return Timing.WaitForSeconds(kill_delay);
        if( this==null || Game.IsLoadingLevel || GameCamera.Get==null )
            yield break;

        foreach( Player p in palive ) {
            p.props["score"] = (int)p.props["score"] + survival_bonus;
        }
        foreach( TeamData td in teams.teams ) {
            if( Players.Get.GetAliveTeam(td.Number).Count>0 )
                rounds_won[td.Number]++;
            if( gamemode==Mode.TDM )
                td.kills = rounds_won[td.Number];
        }
        if( gamemode==Mode.CTF ) {
            // reset flags
            foreach( Flag f in Flag._flags ) {
                f.Master_Return();
            }
        }

        GameCamera.Get.CancelPosition();
        foreach( Player p in Players.Get.players ) {
            if (respawning.IsPlayerInQueue(p.id)) {
                Respawning.RespawnObject ro = respawning.GetPlayerQueue(p.id);
                ro.timeOfAdd = RealTime.realtimeSinceStartup;
                ro.waitSecs = reset_time;
            } else {
                RespawningCommon.SpawnPlayer(p, reset_time + Maths.RandomRange(0.0f, 0.3f));
            }
        }
        GameCamera.Get.Flash(Color.white, 0.08f);

        ended = false;
        if( max_rounds>0 && round>max_rounds ) {
            endConditions.Master_ReachedEndCondition();
            for( int i=0; i<rounds_won.Length; i++ ) rounds_won[i]=0;
        }
    }

}

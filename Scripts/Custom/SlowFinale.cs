using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Teal;

/*
 * author: @AL
 * script version: 0.1
 * target game version: 0.8.60
 */

[DisallowMultipleComponent]
public class SlowFinale : MonoBehaviour
{
    // how much slowing to apply, where 1 is normal speed
    const float slow_amount = 0.48f;

    // how long to remain at the full slow amount
    const float slow_duration = 3.8f;

    // how long it takes for game speed to return to normal
    const float fade_duration = 6.0f;

    bool active;
    float start_time;

    private void Awake()
    {
        Eventor.AddListener(Events.Match_Ended,OnEnd);
    }

    private void OnDestroy()
    {
        Eventor.RemoveListener(Events.Match_Ended,OnEnd);
    }

    void Start()
    {
        active = false;
    }

    void Update()
    {
        if( active ) {
            float dt = RealTime.timeSinceLevelLoad-start_time;
            if( dt<slow_duration ) { // full slow period
                UnityEngine.Time.timeScale = slow_amount;
            } else if( dt<(slow_duration+fade_duration) ) { // fade period
                UnityEngine.Time.timeScale = slow_amount + (1-slow_amount)*(dt-slow_duration)/fade_duration;
            } else { // back to normal
                UnityEngine.Time.timeScale = 1;
                active = false;
            }
        }
    }

    void OnEnd(IGameEvent ev)
    {
        active = true;
        start_time = RealTime.timeSinceLevelLoad;
    }
}

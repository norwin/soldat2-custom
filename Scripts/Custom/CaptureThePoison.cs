﻿using System.Linq;
using System.Collections;
using UnityEngine;
using Newtonsoft.Json;
using Teal;

/*
 * author: @noerw
 * script version: 0.1
 * target game version: 0.8.60
 */

[RequireComponent(typeof(Match))]
[RequireComponent(typeof(Teams))]
[DisallowMultipleComponent]
public class CaptureThePoison : MonoBehaviour
{
    [JsonProperty("FlagPoisonDPS")] public float initialPoisonDPS = 0.04F;
    [JsonProperty("PoisonDPSIncreaseOnCap")] public float poisonDPSIncrease = 0.015F;

    private int updateIteration;
    private Controls[] poisonTargets = new Controls[2];
    private float currentPoisonDPS;

    private void Awake()
    {
        Eventor.AddListener(Events.Flag_Grabbed, OnFlagGrabbed);
        Eventor.AddListener(Events.Flag_Captured, OnFlagCaptured);
        Eventor.AddListener(Events.Item_Dropped, OnItemDrop);
        currentPoisonDPS = initialPoisonDPS;
    }

    private void OnDestroy()
    {
        Eventor.RemoveListener(Events.Flag_Grabbed, OnFlagGrabbed);
        Eventor.RemoveListener(Events.Flag_Captured, OnFlagCaptured);
        Eventor.RemoveListener(Events.Item_Dropped, OnItemDrop);
    }

    void FixedUpdate()
    {
        // FixedUpdate is called with 50Hz by default. we want to lifedrain twice per second.
        if (updateIteration++ % 25 != 1)
                return;

        if (currentPoisonDPS != 0)
        {
            if (poisonTargets[0])
                ApplyPoison(poisonTargets[0], currentPoisonDPS / 2);
            if (poisonTargets[1])
                ApplyPoison(poisonTargets[1], currentPoisonDPS / 2);
        }
    }

    void OnFlagCaptured (IGameEvent ev)
    {
        currentPoisonDPS += poisonDPSIncrease;
    }

    void OnFlagGrabbed(IGameEvent ev)
    {
        Controls flagger = GetFlagHolder(ev.Sender);
        if (!flagger)
            return;

        if (currentPoisonDPS != 0.0)
        {
            Flag flag = ev.Sender.GetComponent<Flag>();
            poisonTargets[flag.team.Number] = flagger;
        }
    }

    void OnItemDrop(IGameEvent ev)
    {
        Flag flag = ev.Sender.GetComponent<Flag>();
        if (!flag)
            return;

        if (currentPoisonDPS != 0.0)
            poisonTargets[flag.team.Number] = null;
    }


    void ApplyPoison(Controls target, float amount)
    {
        StandardObject obj = target.GetComponent<StandardObject>();
        float health = obj.healthDisplay;
        if (health >= amount)
            obj.Master_SetHealth(health - amount);
        else
            obj.Master_Death();
    }

    Controls GetFlagHolder(GameObject flagObject)
    {
        Flag flag = flagObject.GetComponent<Flag>();
        ItemPickup holder = flag.lastholder;
        if (holder)
            return holder.GetComponent<Controls>();
        return null;
    }
}


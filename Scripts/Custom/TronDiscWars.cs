using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Teal;
using Newtonsoft.Json;

/*
 * Accompanying script for a_DiscWars modifier:
 * - adds drag to throwables after a timeout
 * - sets a cyan outline on knifes
 * author: @noerw
 * script version: 0.1
 * target game version: 0.8.60
 */

[DisallowMultipleComponent]
public class TronDiscWars : MonoBehaviour
{
    [JsonProperty("SlowKnifesAfter")] public float slowAfterSecs = 3.5f;
    public Color cyan = new Color(0.0f, 0.8f, 0.86f, 0.8f);

    private Dictionary<int, IEnumerator> knifeCoroutines = new Dictionary<int, IEnumerator>();
    private float originalKnifeDrag = -1.0f;
    private float originalKnifeAngularDrag = -1.0f;

    private void Awake() {
        Eventor.AddListener(Events.Item_Dropped, OnItemDropped);
        Eventor.AddListener(Events.Item_Picked, OnItemPicked);
        Eventor.AddListener(Events.Created, OnCreated);
    }

    private void OnDestroy() {
        Eventor.RemoveListener(Events.Item_Dropped, OnItemDropped);
        Eventor.RemoveListener(Events.Item_Picked, OnItemPicked);
        Eventor.RemoveListener(Events.Created, OnCreated);
    }

    // apply static outline color to knifes
    void OnCreated (IGameEvent ev) {
        if (Net.IsDedicated) return;
        if (ev.Sender.gameObject.name != "Knife") return;

        Renderer r = ev.Sender.GetComponentInChildren<Renderer>();
        ColorMaterial colorMaterial = r.gameObject.GetComponent<ColorMaterial>();
        if (colorMaterial == null)
            colorMaterial = r.gameObject.AddComponent<ColorMaterial>();

        colorMaterial.SetOutlineSize(0.1f);
        colorMaterial.SetOutlineColor(cyan);
    }

    // remove slowdown applied via SlowdownKnifeDelayed
    void OnItemPicked (IGameEvent gameEvent) {
        GlobalItemEvent ev = (GlobalItemEvent)gameEvent;
        Throwable throwable = ev.Item.GetComponentInChildren<Throwable>();
        if (throwable == null) return;
        int id = ev.Item.gameObject.GetInstanceID();
        if (knifeCoroutines.ContainsKey(id)) {
            StopCoroutine(knifeCoroutines[id]);
            knifeCoroutines.Remove(id);
        };
        Rigidbody2D phys = ev.Item.GetComponent<Rigidbody2D>();
        phys.drag = originalKnifeDrag;
        phys.angularDrag = originalKnifeAngularDrag;
    }

    // apply trail color to throwable based on gostek color
    void OnItemDropped (IGameEvent gameEvent) {
        // only apply on throwables
        GlobalItemEvent ev = (GlobalItemEvent)gameEvent;
        Throwable throwable = ev.Item.GetComponentInChildren<Throwable>();
        if (throwable == null) return;

        IEnumerator slowdown = SlowdownKnifeDelayed(slowAfterSecs, throwable);
        knifeCoroutines.Add(ev.Item.gameObject.GetInstanceID(), slowdown);
        StartCoroutine(slowdown);
    }

    IEnumerator SlowdownKnifeDelayed(float delay, Throwable t) {
        yield return new WaitForSeconds(delay);
        GostekPickup holder = t.GetComponentInParent<GostekPickup>();
        Rigidbody2D phys = t.GetComponent<Rigidbody2D>();
        if (originalKnifeDrag == -1.0f) originalKnifeDrag = phys.drag;
        if (originalKnifeAngularDrag == -1.0f) originalKnifeDrag = phys.angularDrag;
        phys.drag = 0.7f;
        phys.angularDrag = 0.7f;
    }
}

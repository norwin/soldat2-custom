using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Teal;
using Newtonsoft.Json;

/*
 * Workaround to make the trails of throwables also scaleable, similar to
 * GameplaySettings.BulletTrailWidthScale
 * author: @noerw
 * script version: 0.1
 * target game version: 0.8.60
 */

[DisallowMultipleComponent]
public class ThrowableTrailSize : MonoBehaviour {
    [JsonProperty("TrailWidthScale")] public float trailWidthScale = 2.0f;

    private void Awake()     { Eventor.AddListener(Events.Created, OnCreated); }
    private void OnDestroy() { Eventor.RemoveListener(Events.Created, OnCreated); }

    void OnCreated (IGameEvent ev) {
        Throwable throwable = ev.Sender.GetComponent<Throwable>();
        if (throwable == null || throwable.trail == null)
            return;
        throwable.trail.startWidth *= trailWidthScale;
        throwable.trail.endWidth   *= trailWidthScale;
		}
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Teal;
using Newtonsoft.Json;

/*
 * Sets the trail color of throwables to a distinctive color of the thrower.
 * author: @noerw
 * script version: 0.1
 * target game version: 0.8.60
 */

[DisallowMultipleComponent]
public class ColoredTrails : MonoBehaviour
{
    [JsonProperty("TeamGamesOnly")] public bool teamGamesOnly = false;
    [JsonProperty("ColorizeBullets")] public bool colorizeBullets = true;
    [JsonProperty("ColorizeThrowables")] public bool colorizeThrowables = true;
    // available values: skin, hat, hair, foot, calf, thigh, pants, chest, upperarm, forearm, hand, pads
    [JsonProperty("ForceColorFromBodyPart")] public string preferredBodyPart = "";

    private string[] bodyparts = {
        "hair color",
        "hat color",
        "skin color",
        "pants color",
        "pads color",
        "hand color",
        "jets color",
        "foot color",
        "chest color",
        "forearm color",
        "upperarm color",
        "calf color",
        "thigh color",
    };
    private Dictionary<ushort, Color> playerColors = new Dictionary<ushort, Color>();
    private Dictionary<Color, Gradient> colorGradients = new Dictionary<Color, Gradient>();
    private bool isTeamsGame = false;

    private void Awake() {
        if ((bool)Teams.instance && Teams.instance.IsTeamsGame())
            isTeamsGame = true;

        if (colorizeThrowables) {
            Eventor.AddListener(Events.Item_Picked, OnItemPicked);
            Eventor.AddListener(Events.Created, OnCreated);
        }
        if (colorizeBullets && !isTeamsGame)
            Eventor.AddListener(Events.Bullet_Created, OnBulletCreated);
    }

    private void OnDestroy() {
        if (colorizeThrowables) {
            Eventor.RemoveListener(Events.Item_Picked, OnItemPicked);
            Eventor.RemoveListener(Events.Created, OnCreated);
        }
        if (colorizeBullets)
            Eventor.RemoveListener(Events.Bullet_Created, OnBulletCreated);
    }

    // apply trail color to throwable based on gostek color
    void OnItemPicked (IGameEvent gameEvent) {
        if (Net.IsDedicated) return;

        // only apply on throwables with trail
        GlobalItemEvent ev = (GlobalItemEvent)gameEvent;
        Throwable throwable = ev.Item.GetComponentInChildren<Throwable>();
        if (throwable == null || throwable.trail == null) return;

        Color color = PlayerColor(ev.Holder.gameObject);
        throwable.trail.startColor = color;
        throwable.trail.endColor = color;
    }

    void OnCreated (IGameEvent ev) {
        Throwable throwable = ev.Sender.GetComponent<Throwable>();
        if (throwable == null || throwable.trail == null) return;
        GostekAppearance gostek = ev.Sender.GetComponentInParent<GostekAppearance>();
        if (gostek == null) return;
        Color color = PlayerColor(gostek.gameObject);
        throwable.trail.startColor = color;
        throwable.trail.endColor = color;
    }

    void OnBulletCreated(IGameEvent gameEvent) {
        StandardBullet bullet = gameEvent.Sender.GetComponent<StandardBullet>();
        if (bullet == null) return;
        Color color = PlayerColor(bullet.owner);
        if (colorGradients.ContainsKey(color))
            bullet.trail.colorGradient = colorGradients[color];
    }

    Color PlayerColor(GameObject gostek) {
        if (isTeamsGame) {
            GostekAppearance appearance = gostek.GetComponent<GostekAppearance>();
            return appearance.team.GetColor();
        } else if (teamGamesOnly) {
          return Color.white;
        }
        Controls ctrl = gostek.GetComponent<Controls>();
        return FindBestPlayerColor(ctrl.player);
    }

    Color FindBestPlayerColor(Player player) {
        if (playerColors.ContainsKey(player.id))
            return playerColors[player.id];

        float maxSaturation = 0.0f;
        Color bestColor = Color.white;
        if (preferredBodyPart != null && preferredBodyPart.Length > 0) {
            bestColor = BodyPartColor(player, preferredBodyPart + " color");
        } else {
            foreach (string bodyPart in bodyparts) {
                float saturation, val;
                Color c = BodyPartColor(player, bodyPart);
                Color.RGBToHSV(c, out _, out saturation, out val);
                if (saturation > maxSaturation) {
                    maxSaturation = saturation;
                    bestColor = c;
                }
            }
        }

        // normalize color a to avoid adding a meta of choosing gostek color
        float hue, satu;
        Color.RGBToHSV(bestColor, out hue, out satu, out _);
        if (satu < 0.5f) bestColor = Color.white;
        else bestColor = Color.HSVToRGB(hue, satu, 0.7f);

        playerColors.Add(player.id, bestColor);
        colorGradients.Add(bestColor, BulletGradient(bestColor));
        return bestColor;
    }

    Gradient BulletGradient(Color color) {
        float alpha = 1.0f;
        Gradient gradient = new Gradient();
        gradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(color, 0.0f), new GradientColorKey(color, 1.0f) },
            new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
        );
        return gradient;
    }

    Color BodyPartColor(Player player, string bodyPart) {
        // access color on player props, not the GostekAppearance,
        // as the latter may be unset during spawning.
        return ColorExtensions.HexToColor((int)player.props[bodyPart]);
    }
}

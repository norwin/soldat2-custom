using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Teal;

/*
 * adds a hotkey ctrl+d to reload scripts and restart the match, to aid script development
 * author: @noerw
 * script version: 0.1
 * target game version: 0.8.66
 */

[DisallowMultipleComponent]
public class ScriptDevelopment: MonoBehaviour {
    string hotkeyWithCtrlReload = "d";

    void Update() {
        if (Net.IsDedicated) return;
        if (Input.GetKey("left ctrl") && Input.GetKeyDown(hotkeyWithCtrlReload))
            RecompileAndRestart();
    }

    bool RecompileAndRestart() {
        if (!MonoSingleton<GameScriptManager>.Get.RecompileAll()) {
            GameChat.ChatOrLog("error compiling scripts, see console (alt+~)");
            return false;
        }
        return RestartMatch();
    }

    bool RestartMatch() {
        if (!Net.IsServer)
            return false;
        if (GamesCycle.instance == null || GamesCycle.instance.game == null)
            return false;
		MonoSingleton<Lobby>.Get.NoLobbyOnNextMatch();
        GamesCycle.Game game = GamesCycle.instance.game;
		GamesCycle.instance.Master_LoadGame(game.rules, game.level, game.modifiers);
        return true;
    }
}

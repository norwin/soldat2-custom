using UnityEngine;
using Teal;

/*
 * This class gives bots versatile random movement. It should be used with other bot behaviours disabled.
 
 * author: @RetiredSnail
 * script version: 1.1
 * target game version: 0.8.60
 */

[DisallowMultipleComponent]
public class FunkyBots : MonoBehaviour
{
	private const int STATE_LEFT = 0;
	private const int STATE_RIGHT = 1;
	private const int STATE_JUMPING = 2;
	private const int STATE_JETTING = 3;
	
	private const int NUM_STATES = 4;
	
	private int state = STATE_LEFT;
	private int tick = 0;
	
	private System.Random rnd;
	
	public FunkyBots() {
		rnd = new System.Random();
	}
	
	private void nextState() {
		if (rnd.NextDouble() < 0.3) {
			state = rnd.Next(NUM_STATES);
		}
		
		if (state == STATE_LEFT)
			state = STATE_RIGHT;
		else if (state == STATE_RIGHT)
			state = STATE_LEFT;
	}
	
    void FixedUpdate()
    {
        if(Net.IsServer) {
			if(tick == 0) {
				nextState();
			}
			
			tick++;
					
            foreach (Player bot in Players.Get.GetBots()) {
                if(bot && bot.controlled) {
					if (state == STATE_LEFT) 
						bot.controlled.SetAxis(Axis.H, -1.0f);
					else if (state == STATE_RIGHT)
						bot.controlled.SetAxis(Axis.H, 1.0f);	
					
					if (state == STATE_LEFT || state == STATE_RIGHT) {
						if (tick < 25) {
							bot.controlled.SetKey(Key.Jump, pressed: true);
						} else if (tick < 75) {
							bot.controlled.SetKey(Key.Jets, pressed: true);
						}
					} else if (state == STATE_JUMPING) {
						bot.controlled.SetKey(Key.Jump, pressed: true);
					} else if (state == STATE_JETTING) {
						bot.controlled.SetKey(Key.Jets, pressed: true);
					}
				}
            }
			
			tick %= 100;
        }
    }
}

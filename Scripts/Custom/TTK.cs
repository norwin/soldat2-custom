using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Teal;

/*
 * Tools for measuring time to kill, to be used with z_BotsJustStandThere
 * Available commands: !reset / !r, !autoreset / !a
 * author: @AL
 * script version: 0.1
 * target game version: 0.8.60
 */

[DisallowMultipleComponent]
public class TTK : MonoBehaviour
{
    bool in_progress, finished, autoreset;
    Vector3 p,t;
    float ttk;

    private void Awake()
    {
        in_progress = false;
        finished = false;
        autoreset = false;

        Eventor.AddListener(Events.Bullet_Created,OnShoot);
        Eventor.AddListener(Events.Died,OnDied);
        GameChat.instance.OnChat.AddListener(OnPlayerChat);
    }

    private void OnDestroy()
    {
        Eventor.RemoveListener(Events.Bullet_Created,OnShoot);
        Eventor.RemoveListener(Events.Died,OnDied);
        GameChat.instance.OnChat.RemoveListener(OnPlayerChat);
    }

    void Update()
    {
        if( in_progress )
        {
            if( finished )
            {
                HUD.Get.Notify("T: "+ttk.ToString("N2"), Color.white);
            }
            else
            {
                HUD.Get.Notify("T: "+TimeMeasure.End().ToString("N2"), Color.white);
            }
        }
    }

    void OnPlayerChat(Player p, string msg)
    {
        if( msg=="!reset" || msg=="!r" )
        {
            in_progress = false;
            finished = false;
            GameChat.ChatOrLog("TTK test reset");
        }
        if( msg=="!autoreset" || msg=="!a" )
        {
            autoreset = !autoreset;
            GameChat.ChatOrLog("Autoreset: "+autoreset.ToString());
        }
    }

    void OnShoot(IGameEvent ev)
    {
        Player shooter = ev.Sender.GetComponent<StandardBullet>().owner.GetComponent<Controls>().player;
        Player local = Players.Get.GetLocalPlayer();
        if( shooter == local && !in_progress )
        {
            in_progress = true;
            finished = false;
            TimeMeasure.Start();
            p = ev.Sender.GetComponent<StandardBullet>().transform.position;
            GameChat.ChatOrLog("TTK test started");
        }
    }

    void OnDied(IGameEvent ev)
    {
        if( finished ) return;
        if( autoreset )
        {
            in_progress = false;
        }
        else
        {
            finished = true;
        }
        ttk = TimeMeasure.End();
        GameChat.ChatOrLog("TTK: "+ttk.ToString()+" s");
        t = ev.Sender.GetComponent<Controls>().transform.position;
        GameChat.ChatOrLog("Distance: "+(t-p).magnitude.ToString());
    }
}
